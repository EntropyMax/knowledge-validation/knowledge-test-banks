#!/usr/bin/python3
import os
import re
import pymongo
from bson.objectid import ObjectId

# Keys used in a question json file
id_key = "_id"
rel_id_key = "rel-link_id"
question_name_key = "question_name"
topic_key = "topic"
question_key = "question"
snippet_key = "snippet"
snippet_lang_key = "snippet_lang"
choices_key = "choices"
answer_key = "answer"
explanation_key = "explanation"
group_key = "group"
group_name_key = "group_name"
group_questions_key = "questions"
ksats_key = "KSATs"
ksat_id_key = "ksat_id"

# Names for various files/folders
docs_dir = "docs"
src_dir = "src"
testbanks_dir = "test-banks"
readme_md = "README.md"
summary_md = "SUMMARY.md"
feedback_md = "feedback.md"
question_ext = ".question.json"

testbank_template_url = "https://gitlab.com/90cos/cyv/eval-systems/knowledge-test-bank-system"
gitlab_url = 'https://gitlab.com'
PROJECT_URL = os.getenv('CI_PROJECT_URL', 'https://gitlab.com/local/project')
WEBIDE_URL = f'{gitlab_url}/-/ide/project/{PROJECT_URL.replace("https://gitlab.com/", "")}/edit/master/-/'


HOST = os.getenv('MONGO_HOST', 'localhost')
PORT = int(os.getenv('MONGO_PORT', '27017'))
MTTL_REPO = os.getenv('MTTL_REPO', None)

db_name = 'knowledge_test_bank'

client = pymongo.MongoClient(HOST, PORT)
db_mttl = client.mttl
db = client[db_name]

topic_count = 1
question_count = 1


def write_mdbook_summary(collection_list: list):
    summary_data = None
    summary_path = os.path.join(docs_dir, src_dir, summary_md)
    tab_space = '    '
    study_bank_entries = []
    with open(summary_path) as f:
        summary_data = f.read()
    for collection in collection_list:
        study_bank_entries.append(
            f'{tab_space}- [{collection.replace("_", " ").upper()}]({testbanks_dir}/{collection}/{readme_md})')
    summary_data = re.sub(r'(?<=- \[Study Guides\]\(study-guides/README.md\))(.*)(?=- \[Feedback\]\(feedback.md\))', 
                          '\n'+'\n'.join(study_bank_entries)+'\n', summary_data, flags=re.DOTALL)
    with open(summary_path, 'w') as f:
        f.write(summary_data)


def write_mdbook_feedback_link():
    feedback_data = None
    feedback_path = os.path.join(docs_dir, src_dir, feedback_md)
    with open(feedback_path) as f:
        feedback_data = f.read()
    feedback_data = re.sub(testbank_template_url, PROJECT_URL, feedback_data, flags=re.DOTALL)
    with open(feedback_path, 'w') as f:
        f.write(feedback_data)


def question_answers(question: dict) -> list:
    answer_choices_options = []
    answer_choices_lines = []
    answer_choices = []
    # This is necessary for text that overflows the display. The auto setting applies a scrollbar when needed.
    div_style = '<div style= "overflow-x: auto">'
    for i, answer in enumerate(question[choices_key]):
        answer_choices_options.append(chr(65 + i))
        answer_choices_lines.append(':---')
        answer_choices.append(answer.replace('\n', '<br>'))
    return [
        f'{div_style}\n\n',
        f'|{"|".join(answer_choices_options)}|',
        f'|{"|".join(answer_choices_lines)}|',
        f'|{"|".join(answer_choices)}|\n\n',
        f'</div>\n',
    ]


def append_question(path:str, filename:str, question:object, topic_count:int, question_count:int, ksat_list:list, 
                    group:str=None, collection_path=''):
    question_template = []
    question_path = f'{question[topic_key]}/{question[question_name_key]}{question_ext}'
    group_statement = ''
    snippet_statement = ''
    if group:
        group_statement = f'GROUP {group}: '
        snippet_statement = f'<a href="#begin-{group.lower()}-questions"> Snippet Here</a>'
    if snippet_key in question and not group:
        question_template.append(f'```{question[snippet_lang_key]}\n{question[snippet_key]}\n```\n\n')
    question_template += [
        f'<h3><a class="header" href="#{question[id_key]}" id="{question[id_key]}">{topic_count}.{question_count} ' \
        f'{group_statement}{question[question_key]}{snippet_statement}</a></h3>\n\n'
    ]

    question_template += question_answers(question)
    
    # hidden answer section
    question_template += [
        '```rust,no_run,noplayground wrap',
        'Reveal the answer by clicking the "eye"',
        f'# Answer: ({chr(65 + question[answer_key])})',
        '#',
        '# **Feedback**'
    ]
    # determine if there is more than one explanation that needs to be listed
    if len(question[explanation_key]) > 1:
        for i, exp in enumerate(question[explanation_key]):
            question_template += [f'# {chr(65 + i)}: {question[explanation_key][i]}']
    else:
        question_template += [f'# {question[explanation_key][0]}']
    
    if len(ksat_list) > 0:
        question_template += ['#', f'# KSATs: {", ".join(ksat_list)}']
    
    question_template += ['```\n']

    question_template += [
        f'[Edit Question]({WEBIDE_URL}{collection_path}/{question_path.replace(" ", "_")}) on GitLab\n\n',
        '<br><br>\n\n---\n\n<br><br>\n',
    ]
    with open(os.path.join(path, filename), 'a') as f:
        f.write('\n'.join(question_template))


def readme_topic(path:str, filename:str, collection:str, topic:str, topic_count:int):
    with open(os.path.join(path, filename), 'a') as f:
        f.write(f'## {topic_count}. **{topic}**\n')
        f.write(f'\[[Back to Top](#{collection.lower().replace("_", "-")}-master-question-file)\]\n\n')


def readme_header(path:str, filename:str, collection:str, topics:list):
    header = [
       f'# {collection.replace("_", " ").title()} Master Question File\n',
       '<style>',
       'code {white-space : pre-wrap !important;}',
       '</style>',
       '\n',
       '### Table of Contents',
    ]
    for i, topic in enumerate(topics):
        header.append(f'{i + 1}. [{topic.title()}](#{i + 1}-{topic.lower().replace(" ", "-")})')
    header.append('\n')

    os.makedirs(path, exist_ok=True)
    with open(os.path.join(path, filename), 'w') as f:
        f.write('\n'.join(header))    


def handle_group_questions(path, filename, collection, topic, collection_path):
    global topic_count, question_count
    for group in db[collection].find({topic_key: topic}).distinct('group.group_name'):
        all_questions = list(db[collection].find({topic_key: topic, 'group.group_name': group}))
        snippet_question = db[collection].find_one({topic_key: topic, 'group.group_name': group, snippet_key: {'$ne':None}})
        group_template = [
            f'### **Begin {group.upper()} Questions**\n',
            f'Please refer to the code snippet below for the following {len(all_questions)} question(s).\n',
            f'```{snippet_question[snippet_lang_key]}\n{snippet_question[snippet_key]}\n```\n\n'
        ]
        with open(os.path.join(path, filename), 'a') as f:
            f.write('\n'.join(group_template))

        for question in all_questions:
            process_question_escapes(question)
            append_question(path, readme_md, question, topic_count, question_count, get_ksats(question), group, 
                            collection_path=collection_path)
            question_count += 1

        group_end_template = [
            f'### End {group.upper()} Questions\n',
            '---\n\n'
        ]        
        with open(os.path.join(path, filename), 'a') as f:
            f.write('\n'.join(group_end_template))


def get_ksats(question: dict) -> list:
    ksat_list = []
    if rel_id_key in question and MTTL_REPO:
        rel_link = db_mttl.rel_links.find_one({id_key: ObjectId(question[rel_id_key])})
        if rel_link:
            for mapping in rel_link[ksats_key]:
                ksat = db_mttl.requirements.find_one({id_key: mapping[ksat_id_key]})
                if ksat:
                    ksat_list.append(ksat[ksat_id_key])
    return ksat_list


def add_md_escape(string: str) -> str:
    escape_chars = ["\\", "`", "*", "_", "{", "}", "[", "]", "(", ")", "#", "+", "-", ".", "!"]

    # Escape all characters with special meaning
    for character in escape_chars:
        string = string.replace(character, f"\\{character}")

    # Now ensure the pipe is escaped for a md table
    string = string.replace("|", "&#124;")

    return string


def process_question_escapes(question: dict) -> None:
    for index, choice in enumerate(question[choices_key]):
        question[choices_key][index] = add_md_escape(choice)


def main():
    global topic_count, question_count
    collection_list = db.list_collection_names()
    for collection in collection_list:
        topic_count = 1
        collection_path = os.path.join(testbanks_dir, collection)
        doc_src_path = os.path.join(docs_dir, src_dir, collection_path)
        topics = db[collection].find().distinct(topic_key)
        # create new README.md file
        readme_header(doc_src_path, readme_md, collection, topics)
        for topic in topics:
            question_count = 1
            # create topic header for each topic
            readme_topic(doc_src_path, readme_md, collection, topic, topic_count)

            # group questions
            handle_group_questions(doc_src_path, readme_md, collection, topic, collection_path)

            for question in db[collection].find({topic_key: topic, group_key: None}):
                process_question_escapes(question)
                append_question(doc_src_path, readme_md, question, topic_count, question_count, get_ksats(question), collection_path=collection_path)
                question_count += 1
            topic_count += 1
    write_mdbook_summary(collection_list)
    write_mdbook_feedback_link()



if __name__ == "__main__":
    main()
