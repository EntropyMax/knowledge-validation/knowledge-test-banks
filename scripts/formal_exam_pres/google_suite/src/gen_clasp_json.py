import argparse
import json

description = "This will generate the necessary .clasp.json file"
script_id_desc = "This is the Google Apps Script ID that will store & execute the formal exam creator."
project_id_desc = "This is the Google Cloud Platform project ID that is associated with the Google Apps Script ID."


def create_clasp_json(args: argparse.Namespace):
    with open(".clasp.json", "w") as clasp_fp:
        json.dump({"scriptId": args.script_id, "projectId": args.project_id}, clasp_fp)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description)
    parser.add_argument("script_id", type=str, help=script_id_desc)
    parser.add_argument("project_id", type=str, help=project_id_desc)
    params = parser.parse_args()
    create_clasp_json(params)
