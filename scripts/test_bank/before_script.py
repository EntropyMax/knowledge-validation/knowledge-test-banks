#!/usr/bin/python3

import os
import json
import subprocess
import pymongo

HOST = os.getenv('MONGO_HOST', 'localhost')
PORT = int(os.getenv('MONGO_PORT', '27017'))

db_name = 'knowledge_test_bank'
test_banks_path = 'test-banks'
client = pymongo.MongoClient(HOST, PORT)
db = client[db_name]

def stage_question(question_data:dict, file_path:str):
    file_name = os.path.basename(file_path)
    question_name = file_name.replace('.question.json', '')
    if 'question_name' not in question_data:
        question_data['question_name'] = question_name
    if 'question_path' not in question_data:
        question_data['question_data'] = file_path
    if 'question_type' not in question_data: 
        question_data['question_type'] = 'knowledge'
    if 'disabled' not in question_data: 
        question_data['disabled'] = False
    if 'provisioned' not in question_data: 
        question_data['provisioned'] = 0
    if 'attempts' not in question_data: 
        question_data['attempts'] = 0
    if 'passes' not in question_data: 
        question_data['passes'] = 0
    if 'failures' not in question_data: 
        question_data['failures'] = 0

def import_file_mongo(tb_name:str, file_path:str):
    with open(file_path) as question_file:
        question_data = json.load(question_file)
        stage_question(question_data, file_path)
        db[tb_name].insert_one(question_data)

def drop_mongo_db():
    cmd = f'mongo --host {HOST} --eval \'db.dropDatabase()\' {db_name}'
    subprocess.run(cmd, shell=True)

def main():
    drop_mongo_db()
    test_bank_dirs = [f.path for f in os.scandir(test_banks_path) if f.is_dir()]
    for test_bank_dir in test_bank_dirs:
        test_bank_name = os.path.basename(test_bank_dir)
        for root, dirs, files in os.walk(test_bank_dir):
            for name in files:
                if 'question.json' in name:
                    # continue
                    import_file_mongo(test_bank_name, os.path.join(root, name))

if __name__ == "__main__":
    main()

