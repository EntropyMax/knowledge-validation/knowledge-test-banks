# Table of Contents
- [Study Guide Setup](#study-guide-setup)
  - [Get the Updated Git Pages Website URL](#get-the-updated-git-pages-website-url)
  - [Update the Knowledge Test Bank Repository's Project Description](#update-the-knowledge-test-bank-repositorys-project-description)

# Study Guide Setup
This component provides a method for people to study for their exam. The study guide is generated in a Markdown Book and is publically available. The information in this guide explains how to setup the Study Guides. 

The GitLab continuous integration/continuous delivery (CI/CD) pipeline makes setting up the study guides simple. The pipeline will gather all the content from your test bank and generate a Markdown Book that is deployed to your knowledge test bank's git pages, which is a static website. To ensure users can access the site, you just need to provide the link to your users. The knowlege test bank template is already setup with the link in the repository's description. However, should you need to update the link you can do so by doing the following. Note, these steps require someone with access to the repository's settings.

\[ [TOC](#table-of-contents) \]

## Get the Updated Git Pages Website URL
- From the repository vertical menu on the left, click `Settings` -> `Pages`.
- The correct link to your GitLab pages is under `Access pages`.

\[ [TOC](#table-of-contents) \]

## Update the Knowledge Test Bank Repository's Project Description
- From the repository vertical menu on the left, click `Settings` -> `General`.
- In the `Project description` box, update the link and descrption as necessary.

\[ [TOC](#table-of-contents) \]
