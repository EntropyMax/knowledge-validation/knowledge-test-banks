# Feedback

To give feedback, [create an issue ticket](https://gitlab.com/90cos/cyv/eval-systems/knowledge-test-bank-system/-/issues/new?issuable_template=Feedback) on the study bank repo. Make sure the ticket is labeled with the `customer` label 
